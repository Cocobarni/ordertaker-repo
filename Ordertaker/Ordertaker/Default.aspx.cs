using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace Ordertaker
{
    public partial class _Default : System.Web.UI.Page
    {
        //protected void upDynamicClock_OnLoad(object sender, EventArgs e)
        //{
        //    lblDynamicClock.Text = DateTime.Now.ToString();
        //}

        

        protected void Page_PreRender(object sender, EventArgs e)
        {
            DateTime cutoff = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 11, 0, 0);
            //DateTime.Now.ToString("h:mm:ss tt");
            //if (DateTime.Now > cutoff) //if current time is > 11:30am
            //    ButtonLogin.Enabled = false;
            //    //ButtonLogin.Enabled = true;
            //else
            //    ButtonLogin.Enabled = true;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //force to https
            if (!Request.IsLocal && !string.Equals(Request.Headers["X-Forwarded-Proto"], "https", StringComparison.InvariantCultureIgnoreCase))
            {   
                string redirectUrl = Request.Url.ToString().Replace("http:", "https:");
                string redirectUrlFixedPort = Regex.Replace(redirectUrl, @".com(.*)/", @".com:443/");
                Response.Redirect(redirectUrlFixedPort, false);
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

            //test for dual connection string
            if (HttpContext.Current.Request.IsLocal)
            {
                if (System.Environment.MachineName.Equals("LPD-0250"))
                {
                    SqlDataSource1.ConnectionString = ConfigurationManager.ConnectionStrings["localLPDConnectionString"].ConnectionString;
                    SqlDataSource2.ConnectionString = ConfigurationManager.ConnectionStrings["localLPDConnectionString"].ConnectionString;
                    SqlDataSource3.ConnectionString = ConfigurationManager.ConnectionStrings["localLPDConnectionString"].ConnectionString;
                }
                else if (System.Environment.MachineName.Equals("P245"))
                {
                    SqlDataSource1.ConnectionString = ConfigurationManager.ConnectionStrings["localP245ConnectionString"].ConnectionString;
                    SqlDataSource2.ConnectionString = ConfigurationManager.ConnectionStrings["localP245ConnectionString"].ConnectionString;
                    SqlDataSource3.ConnectionString = ConfigurationManager.ConnectionStrings["localP245ConnectionString"].ConnectionString;
                }
                else
                {
                    throw new Exception("Unknown computer name");
                }
            }
            else
            {
                SqlDataSource1.ConnectionString = ConfigurationManager.ConnectionStrings["dbf7181123fd1f4cf9890aa40a0036b873ConnectionString"].ConnectionString;
                SqlDataSource2.ConnectionString = ConfigurationManager.ConnectionStrings["dbf7181123fd1f4cf9890aa40a0036b873ConnectionString"].ConnectionString;
                SqlDataSource3.ConnectionString = ConfigurationManager.ConnectionStrings["dbf7181123fd1f4cf9890aa40a0036b873ConnectionString"].ConnectionString;
            }

            if (!IsPostBack)
            {
                LabelCurrentUsers.Text = "Current online users: " + Application.Get("UserCount").ToString();
                //Page.Response.Write("Current online users: " + Application.Get("UserCount").ToString());
            }
            if (HttpContext.Current.Request.IsLocal)
            {
                LabelCurrentTime.Text = DateTime.UtcNow.ToString("h:mm:ss tt");
            }
            else
            {
                LabelCurrentTime.Text = DateTime.UtcNow.AddHours(8).ToString("h:mm:ss tt");
            }
        }

        protected void ButtonLogin_Click(object sender, EventArgs e)
        {
            #region comment out muna
            //string[,] array1 = { { "SN",    "User Name",    "Password" },
            //                     { "1",     "etmagboo",     "130280" },
            //                     { "2",     "bsvalencia",   "bergs"},
            //                     { "3",     "kaesguerra",   "kim"},
            //                     { "4",     "jddelacruz",   "aldwin"}
            //                   };
            //bool isAuthorize = false;
            //for (int i = 0; i < 5; i++)
            //{
            //    for (int j = 0; j < 3; j++)
            //    {
            //        if (TextBoxUsername.Text == array1[i, j])
            //            if (TextBoxPassword.Text == array1[i, j + 1])
            //            {
            //                isAuthorize = true;
            //            }
            //            else
            //            {

            //            }
            //    }
            //}

            //if (isAuthorize == true)
            //{
            //    LabelLoginError.ForeColor = System.Drawing.Color.Green;
            //    LabelLoginError.Text = "LoginSucces!";

            //}
            //else
            //{
            //    LabelLoginError.ForeColor = System.Drawing.Color.Red;
            //    LabelLoginError.Text = "Wrong Login!";
            //}
           


            //SqlConnection mycon = new SqlConnection("Data Source=LPD-0250;Initial Catalog=ordertaker;Persist Security Info=True;User ID=sa;Password=p@ssw0rd;");
            //mycon.Open();
            //SqlCommand mycommand = mycon.CreateCommand();
            //mycommand.CommandText = "sp_populatemenu";
            //mycommand.CommandType = CommandType.StoredProcedure;
            //mycommand.Parameters.AddWithValue("@dishId", Convert.ToInt32(GridView1.DataKeys[(gvrow.RowIndex)].Value));
            //mycommand.ExecuteNonQuery();
            //mycon.Close();

            //bool isVerified = verifyuser(TextBoxUsername.Text, TextBoxUsername.Text);
            //if (isVerified == true)
            //{
            //    FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(TextBoxUsername.Text, false, 1);
            //    FormsAuthentication.RedirectFromLoginPage(TextBoxUsername.Text, false);
            //}

            //else
            //    LabelLoginError.Text = "Problem with username and password";
            #endregion
        }


        protected bool verifyuser(string userName, string userPassword)
        {
            bool isverified = false;
            SqlConnection mycon;
            //Rics modified code Dec 23, 2014
            //SqlConnection mycon = new SqlConnection("Data Source=LPD-0250;Initial Catalog=ordertaker;Persist Security Info=True;User ID=sa;Password=p@ssw0rd;");
            //SqlConnection mycon = new SqlConnection(ConfigurationManager.AppSettings.Get("remoteSQLconstringKey"));
            if (HttpContext.Current.Request.IsLocal)
            {
                //mycon = new SqlConnection(ConfigurationManager.ConnectionStrings["localLPDConnectionString"].ConnectionString);
                if (System.Environment.MachineName.Equals("LPD-0250"))
                {
                    mycon = new SqlConnection(ConfigurationManager.ConnectionStrings["localLPDConnectionString"].ConnectionString);
                }
                else if (System.Environment.MachineName.Equals("P245"))
                {
                    mycon = new SqlConnection(ConfigurationManager.ConnectionStrings["localP245ConnectionString"].ConnectionString);
                }
                else
                {
                    throw new Exception("Unknown computer name");
                }
            }
            else
            {
                mycon = new SqlConnection(ConfigurationManager.ConnectionStrings["dbf7181123fd1f4cf9890aa40a0036b873ConnectionString"].ConnectionString);
            }
            
            //SqlConnection mycon = new SqlConnection("Data Source=LPD-0250;Initial Catalog=ordertaker;Integrated Security=True");
            
            
            mycon.Open();
            SqlCommand mycommand = mycon.CreateCommand();
            mycommand.CommandText = "sp_login";
            mycommand.CommandType = CommandType.StoredProcedure;
            mycommand.Parameters.AddWithValue("@userName", userName);
            SqlDataReader mydatareader = mycommand.ExecuteReader();
            while (mydatareader.Read() == true)
            {
                if (mydatareader["userPassword"].ToString() == userPassword)
                    isverified = true;
                
            }
            mycon.Close();

            return isverified;
        }

        protected void ButtonLogin_Click1(object sender, EventArgs e)
        {
            bool isVerified = verifyuser(TextBoxUsername.Text, TextBoxPassword.Text);
            if (isVerified == true)
            {
                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(TextBoxUsername.Text, false, 1);
                FormsAuthentication.RedirectFromLoginPage(TextBoxUsername.Text, false);
                string targetURL;
                targetURL = "Order.aspx?";
                targetURL += "myvariable=";
                targetURL += TextBoxUsername.Text;
                Response.Redirect(targetURL);
            }

            else
                LabelLoginError.Text = "Maling username/password po";
        }

        protected string getdate()
        {   
            string ngayon;
            ngayon = DateTime.Now.ToString();
            return ngayon;
        }

     
    }
}
