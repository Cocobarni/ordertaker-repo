<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Ordertaker._Default" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Home Page</title>
</head>
<body>
    <form id="form1" runat="server" defaultfocus="TextBoxUsername" defaultbutton="ButtonLogin">
    <div>
    <span style="color: #ff0000">
            <table>
                <tr>
                    <td style="width: 757px">
            <asp:Label ID="LabelCurrentUsers" runat="server" Font-Bold="False" Width="462px" ForeColor="Green"></asp:Label></td>
                    <td style="width: 493px">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 757px">
                        <strong><span style="color: black; text-decoration: underline">
        WELCOME!</span></strong></td>
                    <td style="width: 493px">
                    </td>
                </tr>
                <tr>
                    <td style="width: 757px">
                        &nbsp;</td>
                    <td style="width: 493px">
                    </td>
                </tr>
                <tr>
                    <td style="width: 757px">
                        <span style="color: black">
        Ikaw ba ay wala pang lunch?</span></td>
                    <td style="width: 493px">
                    </td>
                </tr>
                <tr>
                    <td style="width: 757px">
                        <span style="color: black">
        Kung oo, tara na't umorder dito sa Ordertaker Website :)</span></td>
                    <td style="width: 493px">
                    </td>
                </tr>
                <tr>
                    <td style="width: 757px">
                        <span style="color: black">
        Syempre, mag log-in ka muna...</span></td>
                    <td style="width: 493px">
                    </td>
                </tr>
                <tr>
                    <td style="width: 757px">
                        &nbsp;</td>
                    <td style="width: 493px">
                        <span style="color: black">Server Time: </span>
                    </td>
                </tr>
                <tr>
                    <td style="width: 757px">
                        Cut-off order is until 11:00 am of the day (server time)</td>
                    <td style="width: 493px">
                        &nbsp;<asp:Label ID="LabelCurrentTime" runat="server" ForeColor="Black" Text="Label"
                            Width="142px"></asp:Label></td>
                </tr>
            </table>
        </span>
        <table style="width: 862px">
            <tr>
                <td style="width: 26px; height: 12px;">
                </td>
                <td style="width: 205px; height: 12px;">
                    <asp:Label ID="Label1" runat="server" Text="Username:" Width="200px"></asp:Label>&nbsp;</td>
                <td style="width: 733px; height: 12px;">
                </td>
                <td style="width: 187px; height: 12px">
                    <span style="color: #0000ff">
                    Global Orders for Today:</span>
                    <%# getdate() %> </td>
            </tr>
            <tr>
                <td style="width: 26px; height: 29px;">
                </td>
                <td style="width: 205px; height: 29px;">
                    <asp:TextBox ID="TextBoxUsername" runat="server" Width="200px"></asp:TextBox></td>
                <td style="width: 733px; height: 29px;">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxUsername"
                        ErrorMessage="Required po"></asp:RequiredFieldValidator></td>
                <td rowspan="6" style="width: 187px">
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" Width="540px">
                        <Columns>
                            <asp:BoundField DataField="orderUser" HeaderText="orderUser" SortExpression="orderUser"  />
                            <asp:BoundField DataField="orderName" HeaderText="orderName" SortExpression="orderName" DataFormatString="{0:f}" >
                                <ControlStyle Width="300px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="orderQuantity" HeaderText="orderQuantity" SortExpression="orderQuantity" />
                            <asp:BoundField DataField="orderPrice" HeaderText="orderPrice" SortExpression="orderPrice" />
                            <asp:BoundField DataField="orderTime" HeaderText="orderTime" ReadOnly="True" SortExpression="orderTime">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td style="width: 26px; height: 21px">
                </td>
                <td style="width: 205px; height: 21px">
                    <asp:Label ID="Label2" runat="server" Text="Password:" Width="200px"></asp:Label></td>
                <td style="width: 733px; height: 21px">
                </td>
            </tr>
            <tr>
                <td style="width: 26px; height: 20px;">
                </td>
                <td style="width: 205px; height: 20px;">
                    <asp:TextBox ID="TextBoxPassword" runat="server" Width="200px" TextMode="Password"></asp:TextBox></td>
                <td style="width: 733px; height: 20px">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBoxPassword"
                        ErrorMessage="Required po"></asp:RequiredFieldValidator></td>
            </tr>
            <tr>
                <td style="width: 26px; height: 31px;">
                </td>
                <td style="width: 205px; height: 31px;">
                    <asp:Button ID="ButtonLogin" runat="server" OnClick="ButtonLogin_Click1" Text="Login" /><br />
                    <asp:Label ID="LabelLoginError" runat="server" Width="199px" ForeColor="Red"></asp:Label></td>
                <td style="width: 733px; height: 31px">
                </td>
            </tr>
            <tr>
                <td style="width: 26px; height: 18px">
                </td>
                <td style="width: 205px; height: 18px">
                    </td>
                <td style="width: 733px; height: 18px">
                </td>
            </tr>
            <tr>
                <td style="width: 26px; height: 18px">
                </td>
                <td colspan="2" style="height: 18px">
                    &nbsp;<span style="color: #0000ff">Order Summary:<br />
                    </span>
        <asp:GridView ID="GridView2" runat="server" DataSourceID="SqlDataSource2" AutoGenerateColumns="False" Width="283px" CellPadding="4" ForeColor="#333333" GridLines="None">
            <Columns>
                <asp:BoundField DataField="orderDate" HeaderText="orderDate" ReadOnly="True" SortExpression="orderDate" />
                <asp:BoundField DataField="orderUser" HeaderText="orderUser" SortExpression="orderUser" />
                <asp:BoundField DataField="orderTotal" HeaderText="orderTotal" ReadOnly="True" SortExpression="orderTotal" >
                    <ItemStyle Font-Bold="True" ForeColor="Red" HorizontalAlign="Center" />
                </asp:BoundField>
            </Columns>
            <RowStyle BackColor="#E3EAEB" />
            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#7C6F57" />
            <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
                </td>
            </tr>
        </table>
        &nbsp;&nbsp;&nbsp;&nbsp;<br />
        &nbsp; Order Count Summary:<asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource3" CellPadding="4" ForeColor="#333333" GridLines="None" Width="338px">
            <Columns>
                <asp:BoundField DataField="totalPrice" HeaderText="totalPrice" ReadOnly="True" SortExpression="totalPrice" />
                <asp:BoundField DataField="totalQuantity" HeaderText="totalQuantity" ReadOnly="True"
                    SortExpression="totalQuantity" />
                <asp:BoundField DataField="orderName" HeaderText="orderName" SortExpression="orderName" />
            </Columns>
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#999999" />
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="Data Source=LPD-0250;Initial Catalog=ordertaker;Persist Security Info=True;User ID=sa;Password=p@ssw0rd"
                        ProviderName="System.Data.SqlClient" SelectCommand="sp_getglobalorders" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="Data Source=LPD-0250;Initial Catalog=ordertaker;Persist Security Info=True;User ID=sa;Password=p@ssw0rd" ProviderName="System.Data.SqlClient" SelectCommand="sp_getorderTotalperUserfortheday" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="Data Source=LPD-0250;Initial Catalog=ordertaker;Persist Security Info=True;User ID=sa;Password=p@ssw0rd"
            ProviderName="System.Data.SqlClient" SelectCommand="sp_getordercountsummary"
            SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </div>
    </form>
</body>
</html>
