<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewOrder.aspx.cs" Inherits="Ordertaker.ViewOrder" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Label ID="Label1" runat="server" Text="Label" Width="272px"></asp:Label><br />
        <br />
        Here are your orders as of today:<br />
        <br />
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="orderId"
            DataSourceID="SqlDataSource1">
            <Columns>
                <asp:BoundField DataField="orderId" HeaderText="orderId" InsertVisible="False" ReadOnly="True"
                    SortExpression="orderId" />
                <asp:BoundField DataField="orderName" HeaderText="orderName" SortExpression="orderName" />
                <asp:BoundField DataField="orderQuantity" HeaderText="orderQuantity" SortExpression="orderQuantity" />
                <asp:BoundField DataField="orderPrice" HeaderText="orderPrice" SortExpression="orderPrice" />
            </Columns>
        </asp:GridView>
    
    </div>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="Data Source=LPD-0098;Initial Catalog=ordertaker;Persist Security Info=True;User ID=testdb;Password=qweasd"
            ProviderName="System.Data.SqlClient" SelectCommand="sp_getorderfortheday" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="Label1" Name="orderUser" PropertyName="Text" Type="String" />
            </SelectParameters>
        </asp:SqlDataSource>
    </form>
</body>
</html>
