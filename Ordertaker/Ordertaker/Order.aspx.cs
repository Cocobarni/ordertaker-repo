using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Web.Configuration;
using System.Transactions;
using RestSharp;
using System.Text.RegularExpressions;

namespace Ordertaker
{
    public partial class Order : System.Web.UI.Page
    {
      protected void Page_PreRender(object sender, EventArgs e)
        {
            if (Request.QueryString["myvariable"] != null)
            {
                LabelGreeting.Text = Request.QueryString["myvariable"].ToString();
            }
            if (LabelGreeting.Text != "etmagboo")
            {
                LinkButtonBack.Visible = false;
                LinkButtonCustomScript.Visible = false;
            }
            else
            {
                LinkButtonBack.Visible = true;
                LinkButtonCustomScript.Visible = true;
            }

            //test for count order of user
            int sqlreturn = 0;
            //SqlConnection mycon = new SqlConnection("Data Source=LPD-0250;Initial Catalog=ordertaker;Persist Security Info=True;User ID=sa;Password=p@ssw0rd;");
            //SqlConnection mycon = new SqlConnection(ConfigurationManager.ConnectionStrings["dbf7181123fd1f4cf9890aa40a0036b873ConnectionString"].ConnectionString);  
            //SqlConnection mycon = new SqlConnection("Data Source=LPD-0250;Initial Catalog=ordertaker;Integrated Security=True");  
            SqlConnection mycon;
            if (HttpContext.Current.Request.IsLocal.Equals(true))
	        {
                //mycon = new SqlConnection(ConfigurationManager.ConnectionStrings["localLPDConnectionString"].ConnectionString);		 
                if (System.Environment.MachineName.Equals("LPD-0250"))
                {
                    mycon = new SqlConnection(ConfigurationManager.ConnectionStrings["localLPDConnectionString"].ConnectionString);		 
                }
                else if (System.Environment.MachineName.Equals("P245"))
                {
                    mycon = new SqlConnection(ConfigurationManager.ConnectionStrings["localP245ConnectionString"].ConnectionString);		 
                }
                else
                {
                    throw new Exception("Unknown computer name");
                }
            }
            else
            {
                mycon = new SqlConnection(ConfigurationManager.ConnectionStrings["dbf7181123fd1f4cf9890aa40a0036b873ConnectionString"].ConnectionString);
            }
            
            mycon.Open();
            SqlCommand mycommand = mycon.CreateCommand();
            mycommand.CommandText = "sp_countorderofuserfortheday";
            mycommand.CommandType = CommandType.StoredProcedure;
            mycommand.Parameters.AddWithValue("@orderUser", LabelGreeting.Text);
            sqlreturn = (Int32)mycommand.ExecuteScalar();
            mycon.Close();

            LabelOrderInfo.Text = "Hello " + LabelGreeting.Text + " you have <b>" + sqlreturn.ToString() + "</b> order(s) for today.";
            
            //for clear order button validation
            TextBoxOrderCount.Text = sqlreturn.ToString();

            //for cut-off disabled submit & clear order buttons
            //DateTime serverTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 11, 0, 0);
            DateTime serverPhTime = DateTime.UtcNow.AddHours(8);
            //if (serverPhTime <  && LabelGreeting.Text == "etmagboo")
            //{
                
            //}
            //Get orderDealerSummary
            if (LabelGreeting.Text.Equals("etmagboo"))
            {
                DataTable myDataTable = GetOrderCountDealerSummary();
                string smsDetailsPartial = ConvertDataTableToScalarVariable(myDataTable);
                if (smsDetailsPartial != null)
                {
                    TextBoxSmsDetails.Text = "Order po: \n";
                    TextBoxSmsDetails.Text += smsDetailsPartial;
                    TextBoxSmsDetails.Text += "\n";
                    TextBoxSmsDetails.Text += "Est: " + GetTotalEstimate().ToString() + "\n";
                    TextBoxSmsDetails.Text += "Cash: \n";
                    TextBoxSmsDetails.Text += "Tnx \n\n";
                    TextBoxSmsDetails.Text += "Seq.X";
                }
            }

            HideSMSView(LabelGreeting.Text);
      }
       
        private string ConvertDataTableToScalarVariable(DataTable myDataTable)
        {
            string myStringTable = string.Empty;
            foreach (var item in myDataTable.Select())
	        {
                for (int i = 0; i < item.ItemArray.Length; i++)
                {
                    myStringTable += item.ItemArray.GetValue(i) + " ";
                }
                myStringTable += "\n";
	        }
            return myStringTable;
        }

        private int GetTotalEstimate()
        {
            int totalEstimate = 0;
            string myConnectionString;
            if (HttpContext.Current.Request.IsLocal)
            {
                if (System.Environment.MachineName.Equals("LPD-0250"))
                {
                    myConnectionString = ConfigurationManager.ConnectionStrings["localLPDConnectionString"].ConnectionString;
                }
                else if (System.Environment.MachineName.Equals("P245"))
                {
                    myConnectionString = ConfigurationManager.ConnectionStrings["localP245ConnectionString"].ConnectionString;
                }
                else
                {
                    throw new Exception("Unknown computer name");
                }
            }
            else
            {
                myConnectionString = ConfigurationManager.ConnectionStrings["dbf7181123fd1f4cf9890aa40a0036b873ConnectionString"].ConnectionString;
            }

            using (SqlConnection myCon = new SqlConnection(myConnectionString))
            {
                using (SqlCommand myCommand = new SqlCommand("usp_GetTotalEstimate", myCon))
                {
                    myCommand.CommandType = CommandType.StoredProcedure;
                    myCommand.Connection.Open();
                    
                    SqlParameter retVal = myCommand.Parameters.Add("@return_value", SqlDbType.Int);
                    retVal.Direction = ParameterDirection.ReturnValue;
                    myCommand.ExecuteNonQuery();
                    totalEstimate = (int)retVal.Value;

                }
            }
            return totalEstimate;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //update 'characters remaining:'
            //TextBoxSmsDetails.Attributes.Add("onkeyup", @"CountChars(" + 160 + ", '" + TextBoxSmsDetails.ClientID + "'," + "'characterCount');");
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "myfunction", "<script type='text/javascript'>myFunction(params...);</script>", true);
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "CountChars", "<script type='text/javascript'>CountChars(" + 160 + ", '" + TextBoxSmsDetails.ClientID + "'," + "'characterCount');</script>", true);

            //force to https
            if (!Request.IsLocal && !string.Equals(Request.Headers["X-Forwarded-Proto"], "https", StringComparison.InvariantCultureIgnoreCase))
            {
                string redirectUrl = Request.Url.ToString().Replace("http:", "https:");
                string redirectUrlFixedPort = Regex.Replace(redirectUrl, @".com(.*)/", @".com:443/");
                Response.Redirect(redirectUrlFixedPort, false);
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

            if (HttpContext.Current.Request.IsLocal)
            {
                //SqlDataSource1.ConnectionString = ConfigurationManager.ConnectionStrings["localLPDConnectionString"].ConnectionString;
                if (System.Environment.MachineName.Equals("LPD-0250"))
                {
                    SqlDataSource1.ConnectionString = ConfigurationManager.ConnectionStrings["localLPDConnectionString"].ConnectionString;
                }
                else if (System.Environment.MachineName.Equals("P245"))
                {
                    SqlDataSource1.ConnectionString = ConfigurationManager.ConnectionStrings["localP245ConnectionString"].ConnectionString;
                }
                else
                {
                    throw new Exception("Unknown computer name");
                }
            }
            else
            {
                SqlDataSource1.ConnectionString = ConfigurationManager.ConnectionStrings["dbf7181123fd1f4cf9890aa40a0036b873ConnectionString"].ConnectionString;
            }

            //Hook up event handler for char counter
            TextBoxSmsDetails.Attributes.Add("onkeyup", @"CountChars(" + 160 + ", '" + TextBoxSmsDetails.ClientID + "'," + "'characterCount');");
            
            if (!IsPostBack)
            {
                //try
                //{
                //    //usersaved = Request["TextBoxUsername"].ToString();
                //    usersaved = ((TextBox)PreviousPage.FindControl("TextBoxUsername")).Text;
                //    //usersaved = Request["myuser"].ToString();
                //}
                //catch (NullReferenceException)
                //{
                //    //usersaved = Request["myValue"].ToString();
                //    usersaved = "etmagboo";
                //}

                //string user = "Welcome " + ((TextBox)PreviousPage.FindControl("TextBoxUsername")).Text + ",";
               //LabelGreeting.Text = usersaved;

                BindMPNToCheckBoxList();
                

                for (int i = 0; i < GridView1.Rows.Count; i++)
                {
                    for (int j = 0; j < GridView1.Columns.Count; j++)
                    {
                        if (GridView1.Rows[i].Cells[j].Text == "Rice")
                        {
                            DropDownList ddl = (DropDownList)GridView1.Rows[i].Cells[j].FindControl("DropDownList1");
                            ddl.Items.Add("6");
                            ddl.Items.Add("7");
                            //Rics@P245 modified code - will no longer support other rice domination
                            //ddl.Items.Add("0.5");
                            //ddl.Items.Add("1.5");
                            //ddl.Items.Add("2.5");
                        }
                    }
                }
            }
            //disables dropdown at page load if checkbox is unchecked
            //disables checkboxlist "freebies" at page load if no checkbox is checked
            int nocheckctr = 0;
            foreach (GridViewRow myrows in GridView1.Rows)
            {
                CheckBox chk = (CheckBox)myrows.FindControl("CheckBox1");
                if (chk == null || !chk.Checked)
                {
                    DropDownList ddl = (DropDownList)GridView1.Rows[myrows.RowIndex].FindControl("DropDownList1");
                    ddl.Enabled = false;
                    GridView1.Rows[myrows.RowIndex].Font.Bold = false;
                    GridView1.Rows[myrows.RowIndex].Cells[5+2].Text = "0";
                    nocheckctr++;
                }
                
            }
            if (nocheckctr == GridView1.Rows.Count)
            {
                for (int i = 0; i < CheckBoxList1.Items.Count; i++)
                {
                    CheckBoxList1.Items[i].Selected = false;
                }
                CheckBoxList1.Enabled = false;
                LabelTotal.Text = "0";

                //for range validator null order
                TextBox2.Text = LabelTotal.Text;
            }
            else
            {
                //CheckBoxList1.Enabled = true;
                CheckBoxList1.Enabled = false;
                
                
                //test for required field validator
                //TextBox1.Text = "not null";
            }

            //test
            //Label1.Text = GrandTotal.ToString();
        }
        
        protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {   //enables or disables dropdown based on checkbox
            foreach (GridViewRow myrows in GridView1.Rows)
            {   //if not check
                CheckBox chk = (CheckBox)myrows.FindControl("CheckBox1");
                if (chk == null || !chk.Checked)
                {
                    DropDownList ddl = (DropDownList)GridView1.Rows[myrows.RowIndex].FindControl("DropDownList1");
                    ddl.Enabled = false;
                    GridView1.Rows[myrows.RowIndex].Font.Bold = false;
                    GridView1.Rows[myrows.RowIndex].Cells[5+2].Text = "0";
                }
                else
                {   //if checked
                    DropDownList ddl = (DropDownList)GridView1.Rows[myrows.RowIndex].FindControl("DropDownList1");
                    ddl.Enabled = true;
                    GridView1.Rows[myrows.RowIndex].Font.Bold = true;
                                        
                    int price = Convert.ToInt32(GridView1.Rows[myrows.RowIndex].Cells[1+2].Text);
                    int quantity = Convert.ToInt32(ddl.SelectedItem.Text);
                    int subtotal = price * quantity;
                    GridView1.Rows[myrows.RowIndex].Cells[5+2].Text = subtotal.ToString();
                    
                    //GridView1.Columns[5].FooterText = total.ToString();
                    LabelTotal.Text = (computeTotal(Convert.ToInt32(GridView1.Rows[myrows.RowIndex].Cells[5+2].Text))).ToString();
                    
                    //test for the range validator
                    TextBox2.Text = LabelTotal.Text;
                    
                }

            }
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckBox1_CheckedChanged(sender, e);
        }


        //function for getting total
        int GrandTotal;
        protected int computeTotal(int subtotal)
        {

            GrandTotal += subtotal;
            return GrandTotal;
        }

        //function for passing LabelTotal
        protected string passLabelTotal()
        {
            return LabelTotal.Text;
        }

        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            
            //initialize parameters for stored proc
            string param_orderName = "";
            decimal param_orderQuantity = 0;
            int param_orderPrice = 0;
            string param_orderUser = LabelGreeting.Text;
            //DateTime param_orderDate = DateTime.Now;

            System.IO.StringWriter writer = new System.IO.StringWriter();
            try
            {
                using (TransactionScope myTxnScope = new TransactionScope())
                {
                    DataTable mDataTable = new DataTable("orderListTable");
                    mDataTable.Columns.Add("Order Name");
                    mDataTable.Columns.Add("Quantity");
                    mDataTable.Columns.Add("Price");
                    mDataTable.Columns["Price"].DataType = typeof(int);//Type.GetType("int");
                    foreach (GridViewRow myrow in GridView1.Rows)
                    {
                        CheckBox chkbx = (CheckBox)myrow.FindControl("CheckBox1");
                        if (chkbx != null && chkbx.Checked)
                        {
                            param_orderName = GridView1.Rows[myrow.RowIndex].Cells[0+2].Text;
                            DropDownList ddl = (DropDownList)GridView1.Rows[myrow.RowIndex].FindControl("DropDownList1");
                            param_orderQuantity = Convert.ToDecimal(ddl.SelectedItem.Text);
                            param_orderPrice = Convert.ToInt32(GridView1.Rows[myrow.RowIndex].Cells[5+2].Text);
                            //call stored proc "sp_populateorder"
                            populateorder(param_orderName, param_orderQuantity, param_orderPrice, param_orderUser);
                            mDataTable.Rows.Add(new object[] { param_orderName, param_orderQuantity, param_orderPrice });
                        }
                    }

                    //evaluates checked items in checkboxlist
                    for (int i = 0; i < CheckBoxList1.Items.Count; i++)
                    {
                        if (CheckBoxList1.Items[i].Selected == true)
                        {
                            param_orderName = CheckBoxList1.Items[i].Text;
                            param_orderQuantity = 1;
                            param_orderPrice = 0;
                            //call stored proc "sp_populateorder"
                            populateorder(param_orderName, param_orderQuantity, param_orderPrice, param_orderUser);
                            mDataTable.Rows.Add(new object[] { param_orderName, param_orderQuantity, param_orderPrice });
                        }
                    }
                    mDataTable.Rows.Add(new object[] { null, "Total", mDataTable.Compute("Sum(Price)", "") });
                    //call email
                    emailaction(param_orderUser, ConvertDataTableToHTML(mDataTable));

                    myTxnScope.Complete();
                }
                
            }   
            catch (TransactionAbortedException taex)
            {
                writer.WriteLine("TransactionAbortedException Message: {0}", taex.Message);

            }
            catch (ApplicationException aex)
            {
                writer.WriteLine("ApplicationException Message: {0}", aex.Message);
            }

            //updates the subtotal column values upon clicking submit button
            CheckBox1_CheckedChanged(sender, e);

            //go to Finish.aspx
            string targetURL;
            targetURL = "Finish.aspx?";
            targetURL += "myvariable=";
            targetURL += LabelGreeting.Text;
            Response.Redirect(targetURL);
            
            
           

            //validates null order and failed sql write order
           

            //-----Section 1: for GUI-----
            //int nullctr1=0;
            //int nullctr2=0;
            //bool isnull1=false;
            //bool isnull2=false;
            //foreach (GridViewRow myrow in GridView1.Rows)
            //{
            //    CheckBox chkbx = (CheckBox)myrow.FindControl("CheckBox1");
            //    if (chkbx == null || !chkbx.Checked)
            //    {
            //        //do stuff
            //        nullctr1++;
            //    }
            //}
            //if (nullctr1 == GridView1.Rows.Count)
            //    isnull1 = true;

            //for (int i = 0; i < CheckBoxList1.Items.Count; i++)
            //{
            //    if (CheckBoxList1.Items[i].Selected == false)
            //    {
            //        //do stuff
            //        nullctr2++;
            //    }
            //}
            //if (nullctr2 == CheckBoxList1.Items.Count)
            //    isnull2 = true;

            //if (isnull1 == true && isnull2 == true)
            //    LabelNullOrder.Text = "WARNING: NULL ORDER!";
            //LabelNullOrder.Visible = true;
            //else
            //    LabelNullOrder.Text = null;

        }

        static public int CreateTransactionScope(string connectionString, SqlCommand mSQLCommand)
        {
            //Rics modified code block Dec 22, 2014
            //returns a value > 0 if the transaction is committed, 0 if the  
            // transaction is rolled back
            int returnValue = 0;
            System.IO.StringWriter writer = new System.IO.StringWriter();
            try
            {
                using (TransactionScope mTransactionScope = new TransactionScope())
                {
                    using (SqlConnection mSQLConnection = new SqlConnection(connectionString))
                    {   
                        returnValue = mSQLCommand.ExecuteNonQuery();
                        writer.WriteLine("Rows to be affected by command: {0}", returnValue);
                    }
                    mTransactionScope.Complete();
                }
            }
            catch (TransactionAbortedException taex)
            {
                writer.WriteLine("TransactionAbortedException Message: {0}", taex.Message);

            }
            catch (ApplicationException aex)
            {
                writer.WriteLine("ApplicationException Message: {0}", aex.Message);
            }
            return returnValue;
        }

        public /*static*/ string ConvertDataTableToHTML(DataTable dt)
        {
            string html = "<table>";
            //add header row
            html += "<tr>";
            for (int i = 0; i < dt.Columns.Count; i++)
                html += "<td>" + dt.Columns[i].ColumnName + "</td>";
            html += "</tr>";
            //add rows
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                html += "<tr>";
                for (int j = 0; j < dt.Columns.Count; j++)
                    if (dt.Columns[j].ColumnName.Equals("Quantity"))
                    {
                        if (i == dt.Rows.Count - 1) //for the "Total" footer
                        {
                            html += "<td style='text-align:center'><b>" + dt.Rows[i][j].ToString() + "</b></td>";
                        }
                        else
                        {
                            html += "<td style='text-align:center'>" + dt.Rows[i][j].ToString() + "</td>";
                        }
                        
                    }
                    else if (dt.Columns[j].ColumnName.Equals("Price"))
                    {
                        if (i == dt.Rows.Count-1) //if the price indicates the total price
                        {
                            html += "<td style='text-align:right;color:red'><b>" + dt.Rows[i][j].ToString() + "</b></td>";
                        }
                        else
                        {
                            html += "<td style='text-align:right'>" + dt.Rows[i][j].ToString() + "</td>";
                        }
                    }
                    else
	                {
                        html += "<td>" + dt.Rows[i][j].ToString() + "</td>";
	                }
                    //html += "<td>" + dt.Rows[i][j].ToString() + "</td>";
                html += "</tr>";
            }
            html += "</table>";
            return html;
        }

        public void populateorder(string orderName, decimal orderQuantity, int orderPrice, string orderUser)
        {
            #region Original Code
            
            int rowsaffected = 0;
            //SqlConnection mycon = new SqlConnection("Data Source=LPD-0250;Initial Catalog=ordertaker;Persist Security Info=True;User ID=sa;Password=p@ssw0rd;");
            //SqlConnection mycon = new SqlConnection(ConfigurationManager.ConnectionStrings["dbf7181123fd1f4cf9890aa40a0036b873ConnectionString"].ConnectionString);
            //SqlConnection mycon = new SqlConnection("Data Source=LPD-0250;Initial Catalog=ordertaker;Integrated Security=True");
            SqlConnection mycon;
            if (HttpContext.Current.Request.IsLocal)
            {
                //mycon = new SqlConnection(ConfigurationManager.ConnectionStrings["localLPDConnectionString"].ConnectionString);
                if (System.Environment.MachineName.Equals("LPD-0250"))
                {
                    mycon = new SqlConnection(ConfigurationManager.ConnectionStrings["localLPDConnectionString"].ConnectionString);
                }
                else if (System.Environment.MachineName.Equals("P245"))
                {
                    mycon = new SqlConnection(ConfigurationManager.ConnectionStrings["localP245ConnectionString"].ConnectionString);
                }
                else
                {
                    throw new Exception("Unknown computer name");
                }
            }
            else
            {
                mycon = new SqlConnection(ConfigurationManager.ConnectionStrings["dbf7181123fd1f4cf9890aa40a0036b873ConnectionString"].ConnectionString);
            }

            mycon.Open();
            SqlCommand mycommand = mycon.CreateCommand();
            mycommand.CommandText = "sp_populateorder";
            mycommand.CommandType = CommandType.StoredProcedure;
            mycommand.Parameters.AddWithValue("@orderName", orderName);
            mycommand.Parameters.AddWithValue("@orderQuantity", orderQuantity);
            mycommand.Parameters.AddWithValue("@orderPrice", orderPrice);
            mycommand.Parameters.AddWithValue("@orderUser", orderUser);
            rowsaffected = mycommand.ExecuteNonQuery();
            mycon.Close();
            //validates if SQL INSERT command is successful
            if(rowsaffected==0)
                Server.Transfer("Error.aspx");
            
            #endregion

            #region NotSureIfWillWork Code
            /*
            //Rics modified code Dec 22, 2014
            int rowsaffected = 0;
            string SQLconnectionString = "Data Source=LPD-0250;Initial Catalog=ordertaker;Persist Security Info=True;User ID=sa;Password=p@ssw0rd;";
            using (SqlCommand mycommand = new SqlCommand())
            {
                mycommand.CommandText = "sp_populateorder";
                mycommand.CommandType = CommandType.StoredProcedure;
                mycommand.Parameters.AddWithValue("@orderName", orderName);
                mycommand.Parameters.AddWithValue("@orderQuantity", orderQuantity);
                mycommand.Parameters.AddWithValue("@orderPrice", orderPrice);
                mycommand.Parameters.AddWithValue("@orderUser", orderUser);
                rowsaffected = CreateTransactionScope(SQLconnectionString, mycommand);
            }
            //validates if SQL INSERT command is successful
            if (!(rowsaffected > 0))
                Server.Transfer("Error.aspx");
            */
            #endregion
        }

        protected void LinkButtonLogout_Click(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            //Roles.DeleteCookie();
            Session.Clear();
            Response.Redirect("Default.aspx");
        }

        protected void ButtonClearOrder_Click(object sender, EventArgs e)
        {
            int sqlvalidate;
            string orderUser = LabelGreeting.Text;
            sqlvalidate = clearordersfortheday(orderUser);
            if (sqlvalidate == 0)
                Server.Transfer("Error.aspx");
            else
            {
                LabelClearOrderSuccess.Text = "Clear Orders Successful!";
                emailaction(orderUser, "orderCancelled");
            }
        }


        public int clearordersfortheday(string orderUser)
        {
            int rowsaffected = 0;
            //SqlConnection mycon = new SqlConnection("Data Source=LPD-0250;Initial Catalog=ordertaker;Persist Security Info=True;User ID=sa;Password=p@ssw0rd;");
            //SqlConnection mycon = new SqlConnection(ConfigurationManager.ConnectionStrings["dbf7181123fd1f4cf9890aa40a0036b873ConnectionString"].ConnectionString);
            //SqlConnection mycon = new SqlConnection("Data Source=LPD-0250;Initial Catalog=ordertaker;Integrated Security=True");
            SqlConnection mycon;
            if (HttpContext.Current.Request.IsLocal)
            {
                //mycon = new SqlConnection(ConfigurationManager.ConnectionStrings["localLPDConnectionString"].ConnectionString);
                if (System.Environment.MachineName.Equals("LPD-0250"))
                {
                    mycon = new SqlConnection(ConfigurationManager.ConnectionStrings["localLPDConnectionString"].ConnectionString);
                }
                else if (System.Environment.MachineName.Equals("P245"))
                {
                    mycon = new SqlConnection(ConfigurationManager.ConnectionStrings["localP245ConnectionString"].ConnectionString);
                }
                else
                {
                    throw new Exception("Unknown computer name");
                }
            }
            else
            {
                mycon = new SqlConnection(ConfigurationManager.ConnectionStrings["dbf7181123fd1f4cf9890aa40a0036b873ConnectionString"].ConnectionString);
            }
            
            mycon.Open();
            SqlCommand mycommand = mycon.CreateCommand();
            mycommand.CommandText = "sp_clearordersfortheday";
            mycommand.CommandType = CommandType.StoredProcedure;
            mycommand.Parameters.AddWithValue("@orderUser", orderUser);
            rowsaffected = mycommand.ExecuteNonQuery();
            mycon.Close();
            return rowsaffected;
        }


        //test for email
        public static void emailaction(string orderUser, string tabulatedOrder)
        {
            string sqlreturn = "";
            //SqlConnection mycon = new SqlConnection("Data Source=LPD-0250;Initial Catalog=ordertaker;Persist Security Info=True;User ID=sa;Password=p@ssw0rd;");
            //SqlConnection mycon = new SqlConnection(ConfigurationManager.ConnectionStrings["dbf7181123fd1f4cf9890aa40a0036b873ConnectionString"].ConnectionString);
            //SqlConnection mycon = new SqlConnection("Data Source=LPD-0250;Initial Catalog=ordertaker;Integrated Security=True");
            SqlConnection mycon;
            if (HttpContext.Current.Request.IsLocal)
            {
                //mycon = new SqlConnection(ConfigurationManager.ConnectionStrings["localLPDConnectionString"].ConnectionString);
                if (System.Environment.MachineName.Equals("LPD-0250"))
                {
                    mycon = new SqlConnection(ConfigurationManager.ConnectionStrings["localLPDConnectionString"].ConnectionString);
                }
                else if (System.Environment.MachineName.Equals("P245"))
                {
                    mycon = new SqlConnection(ConfigurationManager.ConnectionStrings["localP245ConnectionString"].ConnectionString);
                }
                else
                {
                    throw new Exception("Unknown computer name");
                }
            }
            else
            {
                mycon = new SqlConnection(ConfigurationManager.ConnectionStrings["dbf7181123fd1f4cf9890aa40a0036b873ConnectionString"].ConnectionString);
            }
            
            mycon.Open();
            SqlCommand mycommand = mycon.CreateCommand();
            mycommand.CommandText = "SELECT userEmail FROM [user] WHERE userName = '" + orderUser + "'";
            mycommand.CommandType = CommandType.Text;
            //SqlDataReader myreader = mycommand.ExecuteReader();
            //while (myreader.Read())
            //{
            //   sqlreturn = myreader["userEmail"].ToString();
            //}
            sqlreturn = (string)mycommand.ExecuteScalar();
            mycon.Close();


            MailMessage newMail = new MailMessage();
            newMail.To.Add(sqlreturn);
            newMail.Subject = "Ordertaker autoemail";

            if (tabulatedOrder.Equals("orderCancelled"))
            {
                newMail.Body =
                 @"Hi " + orderUser + @",
                <br> This is to confirm of your <font color='red'> CANCELLED </font> orders initiated manually.
                <br> Cancelled orders are the following:
                <br>
                <br> {0}
                <br>
                <br> If you wish to order again, just go to the link specified below.
                <br>Thank You!
                <br><HyperLink NavigateUrl=""http://ordertakerbetacloud.apphb.com"">http://ordertakerbetacloud.apphb.com</HyperLink>";
            }
            else
            {
                newMail.Body = String.Format(
                 @"Hi " + orderUser + @",
                <br> This is an auto email as confirmation from your orders.
                <br> The following is your order for this session:
                <br>
                <br> {0}
                <br>
                <br>Thank You!
                <br><HyperLink NavigateUrl=""http://ordertakerbetacloud.apphb.com"">http://ordertakerbetacloud.apphb.com</HyperLink>", tabulatedOrder);
            }
            
            newMail.From = new MailAddress("etmagboo@pjlhuillier.com", "Ordertaker (Rics)");
            newMail.IsBodyHtml = true;

            //SmtpClient SmtpSender = new SmtpClient();
            //SmtpSender.Port = 25;
            //SmtpSender.Host = "192.168.100.164";
            //SmtpSender.Send(newMail);

            //Added code Rics@P245 for actual email
            string emailMessage = newMail.Body;//TextBoxEmailMessage.Text;
            string emailAddress = sqlreturn;//TextBoxEmailAddress.Text;
            IRestResponse mIRestResponse = SendEmailRestAPI(emailAddress, emailMessage);
            //Response.Write(mIRestResponse.Content);
        }

        //Rics@P245 added code - Fix email module by using MailGun
        public static IRestResponse SendEmailRestAPI(string emailAddress, string emailMessage)
        {
            RestClient client = new RestClient();
            client.BaseUrl = new System.Uri("https://api.mailgun.net/v2");
            client.Authenticator = new HttpBasicAuthenticator("api", "key-d9472a2e710e9be9cc9196bb23617cd2");
            RestRequest request = new RestRequest();
            request.AddParameter("domain", "app3cc6343312a3417b860f4bfddcefe40a.mailgun.org", ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter("from", "no-reply-Ordertaker <me@samples.mailgun.org>");
            request.AddParameter("to", emailAddress);
            request.AddParameter("subject", "[Ordertaker] Your order details");
            request.AddParameter("html", emailMessage);
            //request.AddParameter("bcc", "etmagboo@pjlhuillier.com");
            //request.AddParameter("cc", "cmartin@pjlhuillier.com");
            request.AddParameter("cc", "jbarnido@pjlhuillier.com");
            request.Method = Method.POST;
            return client.Execute(request);
        }

        private DataTable GetOrderCountDealerSummary()
        {
            DataTable myDataTable = new DataTable();
            //SqlConnection myCon = new SqlConnection(ConfigurationManager.ConnectionStrings["localLPDConnectionString"].ConnectionString);
            //using(myCon)
            string myConnectionString;
            if (HttpContext.Current.Request.IsLocal)
            {
                if (System.Environment.MachineName.Equals("LPD-0250"))
                {
                    myConnectionString = ConfigurationManager.ConnectionStrings["localLPDConnectionString"].ConnectionString;
                }
                else if (System.Environment.MachineName.Equals("P245"))
                {
                    myConnectionString = ConfigurationManager.ConnectionStrings["localP245ConnectionString"].ConnectionString;
                }
                else
                {
                    throw new Exception("Unknown computer name");
                }
            }
            else
            {
                myConnectionString = ConfigurationManager.ConnectionStrings["dbf7181123fd1f4cf9890aa40a0036b873ConnectionString"].ConnectionString;
            }
            
            using(SqlConnection myCon = new SqlConnection(myConnectionString))
            {
                using (SqlCommand myCommand = new SqlCommand("usp_GetOrderCountDealerSummary", myCon))
                {
                    myCommand.CommandType = CommandType.StoredProcedure;
                    myCommand.Connection.Open();
                    SqlDataReader myReader = myCommand.ExecuteReader();
                    myDataTable.Load(myReader);
                }
            }
            return myDataTable;
        }

        protected void ButtonDispatch_Click(object sender, EventArgs e)
        {
            SMSManager mSMSManager = new SMSManager();
            mSMSManager.itexmo("09277607229", TextBoxSmsDetails.Text, "etmagboo7229");

            foreach (ListItem eachItem in CheckBoxListMPN.Items)
            {
                if (eachItem.Selected)
                {
                    mSMSManager.itexmo(eachItem.Value, TextBoxSmsDetails.Text, "etmagboo7229");
                }
            }
        }

        private void BindMPNToCheckBoxList()
        {
            string[] mpnCollection = ConfigurationManager.AppSettings["MPNCollectionKey"].Split(';');
            //CheckBoxList1.DataSource = mpnCollection;
            //CheckBoxList1.DataBind();
            foreach (var eachMPN in mpnCollection)
            {
                CheckBoxListMPN.Items.Add(eachMPN);
            }
        }

        private void HideSMSView(string userName)
        {
            if (!userName.Equals("etmagboo"))
            {
                td1.Visible = false;
                td2.Visible = false;
            }
        }
    }
}
//Force edit to commit for something and refresh AppHarbor after linking to BitBucket