using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Web.Security;

namespace Ordertaker
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LabelCurrentUsers.Text = "Current online users: " + Application.Get("UserCount").ToString();
                //Page.Response.Write("Current online users: " + Application.Get("UserCount").ToString());
            }
        }

        protected void ButtonLogin_Click(object sender, EventArgs e)
        {
            //#region comment out muna
            //string[,] array1 = { { "SN",    "User Name",    "Password" },
            //                     { "1",     "etmagboo",     "130280" },
            //                     { "2",     "bsvalencia",   "bergs"},
            //                     { "3",     "kaesguerra",   "kim"},
            //                     { "4",     "jddelacruz",   "aldwin"}
            //                   };
            //bool isAuthorize = false;
            //for (int i = 0; i < 5; i++)
            //{
            //    for (int j = 0; j < 3; j++)
            //    {
            //        if (TextBoxUsername.Text == array1[i, j])
            //            if (TextBoxPassword.Text == array1[i, j + 1])
            //            {
            //                isAuthorize = true;
            //            }
            //            else
            //            {

            //            }
            //    }
            //}

            //if (isAuthorize == true)
            //{
            //    LabelLoginError.ForeColor = System.Drawing.Color.Green;
            //    LabelLoginError.Text = "LoginSucces!";

            //}
            //else
            //{
            //    LabelLoginError.ForeColor = System.Drawing.Color.Red;
            //    LabelLoginError.Text = "Wrong Login!";
            //}
            //#region end


            //SqlConnection mycon = new SqlConnection("Data Source=LPD-0098;Initial Catalog=ordertaker;Persist Security Info=True;User ID=testdb;Password=qweasd;");
            //mycon.Open();
            //SqlCommand mycommand = mycon.CreateCommand();
            //mycommand.CommandText = "sp_populatemenu";
            //mycommand.CommandType = CommandType.StoredProcedure;
            //mycommand.Parameters.AddWithValue("@dishId", Convert.ToInt32(GridView1.DataKeys[(gvrow.RowIndex)].Value));
            //mycommand.ExecuteNonQuery();
            //mycon.Close();

            //bool isVerified = verifyuser(TextBoxUsername.Text, TextBoxUsername.Text);
            //if (isVerified == true)
            //{
            //    FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(TextBoxUsername.Text, false, 1);
            //    FormsAuthentication.RedirectFromLoginPage(TextBoxUsername.Text, false);
            //}

            //else
            //    LabelLoginError.Text = "Problem with username and password";

        }


        protected bool verifyuser(string userName, string userPassword)
        {
            bool isverified = false;
            SqlConnection mycon = new SqlConnection("Data Source=LPD-0098;Initial Catalog=ordertaker;Persist Security Info=True;User ID=testdb;Password=qweasd;");
            mycon.Open();
            SqlCommand mycommand = mycon.CreateCommand();
            mycommand.CommandText = "sp_login";
            mycommand.CommandType = CommandType.StoredProcedure;
            mycommand.Parameters.AddWithValue("@userName", userName);
            SqlDataReader mydatareader = mycommand.ExecuteReader();
            while (mydatareader.Read() == true)
            {
                if (mydatareader["userPassword"].ToString() == userPassword)
                    isverified = true;
                
            }
            mycon.Close();

            return isverified;
        }

        protected void ButtonLogin_Click1(object sender, EventArgs e)
        {
            bool isVerified = verifyuser(TextBoxUsername.Text, TextBoxPassword.Text);
            if (isVerified == true)
            {
                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(TextBoxUsername.Text, false, 1);
                FormsAuthentication.RedirectFromLoginPage(TextBoxUsername.Text, false);
                string targetURL;
                targetURL = "Order.aspx?";
                targetURL += "myvariable=";
                targetURL += TextBoxUsername.Text;
                Response.Redirect(targetURL);
            }

            else
                LabelLoginError.Text = "Maling username/password po";
        }

        protected string getdate()
        {   
            string ngayon;
            ngayon = DateTime.Now.ToString();
            return ngayon;
        }


    }
}
